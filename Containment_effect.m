%% Effect of containment policy
clear; 

f_contain_grid = [0:2:100]/100;
ngrid = numel(f_contain_grid);
lw = 2; % line width

for countries={'NL','CH','IT'}
  close all;
  
  country = countries{1};
  
  hfp=figure(1); clf;
  hs(1) = subplot(1,3,1); set(hs(1),'position',[0.07 0.15 0.25 0.65])
  hs(2) = subplot(1,3,2); set(hs(2),'position',[0.39 0.15 0.25 0.65])
  hs(3) = subplot(1,3,3); set(hs(3),'position',[0.73 0.15 0.25 0.65])
  
  styles = {'-','--','-.'};
  
  f_reg_grid = [0.5 0.1 0.01]; %
  for jj=1:numel(f_reg_grid)
    f_reg = f_reg_grid(jj);
    sty = styles{jj};
    
    %% Run cases
    for ii=1:ngrid
      f_contain = f_contain_grid(ii);
      outs(ii) = SEIRD_model('f_contain',f_contain,'Nsim',500,'country',country,'disp',0,...
        'f_reg',f_reg);
    end
    
    %% Effect plots
    figure(hfp)
    ax=hs(1);
    for ii=1:ngrid
      HH(ii)=max(outs(ii).H);
    end
    plot(ax,f_contain_grid*100,HH/1e3,'linestyle',sty,'linewidth',lw,...
      'DisplayName',sprintf('Registered/Total cases = %02.1f%%',f_reg*100)); hold(ax,'on')
    xlabel(ax,'% of prevented infections')
    title(ax,'Peak of patients in hospital [1000s]')
    legend(ax,'show','location','best')
    
    ax=hs(2);
    for ii=1:ngrid
      DD(ii)=outs(ii).D(end);
    end
    plot(ax,f_contain_grid*100,DD/1e3,'linestyle',sty,'linewidth',lw); hold(ax,'on')
    xlabel(ax,'% of prevented infections')
    title(ax,'Deceased at end of pandemic [1000s]')
    
    ax=hs(3);
    for ii=1:ngrid
      date_end(ii)=outs(ii).t(end);
    end
    plot(ax,f_contain_grid*100,date_end,'linestyle',sty,'linewidth',lw); hold(ax,'on')
    xlabel(ax,'% of prevented infections')
    title(ax,'Date of end of pandemic')
    axis(ax,'tight')
    set(ax,'YTick',linspace(min(get(ax,'Ylim')),max(get(ax,'Ylim')),6));
    set(ax,'YTickLabel',datestr(get(ax,'YTick'),'dd-mmm-yy'));
    
    textstr = sprintf('Effect of containment measures depending of %% of infections prevented \n with respect to non-containment case. Country: %s',country);
    ha=annotation('textbox',[0.2 0.9 0.9 0.1]);
    set(ha,'string',textstr,'FontSize',12,'EdgeColor','None');
    
    %% Trajectory plots
    hft=figure(2);
    ax=gca;
    set(ax,'ColorOrderIndex',1);
    iplotgrid =round(fliplr(52-logspace(0,log10(ngrid),5)));
    for iii=1:numel(iplotgrid)
      if ~exist('hpp','var'), hpp=zeros(numel(iplotgrid),numel(f_reg_grid)); end
      ii = iplotgrid(iii);
      out = outs(ii);
      dispstr = sprintf('f_{cont}=%3.0f%%, f_{reg}=%2.0f%% D=%4.1fk',...
        100*f_contain_grid(ii),100*f_reg,out.D(end)/1e3);
      hpp(iii,jj) = semilogy(ax,out.t,out.C,'displayname',dispstr,'linestyle',sty,'linewidth',lw);
      hold(ax,'on');
    end
    set(ax,'YLim',logspace(1,7,2))
    title(ax,sprintf('Patients in hospital for different f_{reg}(registered/true cases) \n and f_{cont}(containment efficacy). Country = %s',country));
    hc=plot(ax,out.CountryData.DateContain*[1 1],...
      ax.YLim,'k--','DisplayName','start of containment',...
      'linestyle',sty);
    %semilogy(ax,out.t,out.CountryData.HospitalCapacity*ones(size(out.t)),'DisplayName','Hospital capacity');
    ylabel(ax,'N patients (log)')
    xlabel(ax,'Date')
    set(ax,'XTick',linspace(min(get(ax,'XTick')),max(get(ax,'XTick')),6))
    set(ax,'XTickLabel',datestr(get(ax,'XTick'),'dd-mmm-yy'));
    shg
  end
  legend(hpp(:),'location','northeast')
  %% Print 1
  set(hfp,'position',[800 800 750 250]);
  set(hfp,'paperpositionmode','auto')
  fname = fullfile('plots',sprintf('Containment_policy_effects_%s',country));
  print(hfp,'-djpeg',fname);
  
  %% Print trajectories
  figure(hft)
  set(hft,'position',[800 800 450 350]);
  set(hft,'paperpositionmode','auto')
  fname = fullfile('plots',sprintf('Flatten_the_curve_%s',country));
  print(hft,'-djpeg',fname);
  
end