%% COVID-19 Simulation - Overview plots
function hf = plot_overview(country,varargin)
%% Simulation parameters

%% Run Simulation
out = SEIRD_model('country',country,varargin{:});

%% Setup figure
figure(1); clf
nr = 2; nc=3;
hs = zeros(nr,nc);

%% Registered/true cases model vs data
hf = figure;
hs(1)=subplot(nr,nc,1);
ax = hs(1);
hpp(1) = semilogy(out.CountryData.Confirmed.Dates,out.CountryData.Confirmed.Value,'x',...
  'DisplayName','Data'); hold on;
hpp(2) = semilogy(out.t,out.G,'DisplayName','Confirmed cases - Model'); 
hpp(3) = semilogy(out.t,out.I,'DisplayName','True cases - Model'); 

ylim = get(ax,'YLim');
hpp(4) = plot(ax,out.CountryData.DateContain*[1 1],ylim,'Color',[0.5 0.5 0.5],...
  'DisplayName','Containment start');
title(sprintf('Registered cases'));

ylabel(sprintf('prevent %2.0f%%',100*out.P.f_contain))
xlabel('Date'); 
legend(hpp,'location','best');
grid(ax,'on');axis(ax,'tight');

%% Recovered
hs(2)=subplot(nr,nc,2);
ax=gca;
semilogy(out.t,out.H); hold on;
%semilogy(out.t,out.CountryData.HospitalCapacity*ones(size(out.t)),'Color',[0.5 0.5 0.5],'linewidth',2);
semilogy(out.CountryData.Recovered.Dates,out.CountryData.Recovered.Value,'x')
set(ax,'YLim',logspace(1,5,2))
set(ax,'YTick',logspace(1,5,5));

axis(ax,'tight');
plot(ax,out.CountryData.DateContain*[1 1],get(ax,'YLim'),'Color',[0.5 0.5 0.5],...
  'DisplayName','Containment start');

title('People in Hospital')

xlabel('time [days]'); 
legend({'model'},'location','best');
grid(ax,'on');

%% Total deaths
hs(3) = subplot(nr,nc,3);
ax = hs(3);
semilogy(out.t,out.D); hold on;
semilogy(out.CountryData.Deaths.Dates,out.CountryData.Deaths.Value,'x')
title(sprintf('Total Deceased, final = %3.1fk',out.D(end)/1000))

plot(ax,out.CountryData.DateContain*[1 1],get(ax,'YLim'),'Color',[0.5 0.5 0.5],...
  'DisplayName','Containment start');
grid(ax,'on');axis(ax,'tight');

%% Reservoirs
hs(4) = subplot(nr,nc,4);
hp(1)=semilogy(out.t,out.S,'.','DisplayName','S');hold on
hp(2)=semilogy(out.t,out.E,'s','DisplayName','E');
hp(3)=semilogy(out.t,out.I,'o','DisplayName','I'); 
hp(4)=semilogy(out.t,out.R,'+','DisplayName','R');
hp(5)=semilogy(out.t,out.D,'x','DisplayName','D');

ylim = get(gca,'YLim');
plot(out.CountryData.DateContain*[1 1],ylim,'k--');
legend(hp,'location','Best');
set(gca,'Ylim',logspace(2,7,2))
title('Reservoirs');
xlabel('time [days]');

%% Ratio registered/deceased patients
hs(5) = subplot(nr,nc,5);
plot(out.t,out.D./(out.ICum*out.P.f_reg)*100,'DisplayName','Ratio Deceased/Registered'); 
hold on;
plot(out.t,out.D./(out.D+out.R*out.P.f_reg)*100,'DisplayName','Ratio Deceased/Closed')
plot(out.t,out.D./(out.D+out.R)*100,'DisplayName','Ratio Deceased/True cases')

title('Mortality rate [%]')
legend('show','location','best')

%% Fraction of population having suffered disease
hs(6) = subplot(nr,nc,6);
plot(out.t,(out.R+out.D)./out.CountryData.Population*100);
title('% of population exposed')

%% All axis cosmetics
set(hs,'XLim',out.t(1)+[0,out.P.Nsim])
set(hs,'XTick',out.t(1)+linspace(0,out.P.Nsim,6))
for iax = 1:numel(hs)
  set(hs(iax),'XTickLabel',datestr(get(hs(iax),'XTick'),'ddmmm'));
end

textstr = sprintf('Date: %s, Country: %s,  registered/true=%2.0f%%, hospital/registered=%2.0f%%, fatal/registered=%2.0f%%, f_{contain}=%2.0f%%',...
datestr(now,'dd.mm.yy'),country,out.P.f_reg*100,out.P.f_hosp*100,out.P.f_fatal*100,out.P.f_contain*100);

ha = annotation('textbox',[0.1 0.9 0.8 0.1]);
ha.String = textstr;
ha.FontSize = 11;
ha.EdgeColor = 'none';
ha.HorizontalAlignment = 'center';
return
