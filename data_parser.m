function CountryData = data_parser(country,doplot)
if nargin==1
  doplot = false;
end


dataDir = fullfile('covid-19-data','csse_covid_19_data','csse_covid_19_time_series');

Files = {'Confirmed','Deaths','Recovered'};
Nmin = {20,2,2}; % minimum number of cases to start time series
 
for ifile = 1:numel(Files)
  %% load CSV data
  myCase = Files{ifile};
  filePath = fullfile(dataDir,sprintf('time_series_19-covid-%s.csv',myCase));
  D=importdata(filePath);
  %%
  CountryList=D.textdata(2:end,2);
  iCountry = find(contains(CountryList,country));
  if isempty(iCountry)
    error('Country %s not found in data',country);
  end
  Dates = datenum(D.textdata(1,5:end));
  Cases = D.data(iCountry,3:end);
  
  % Remove cases where same number was reported twice or <N cases
  iSame = diff([Cases(1),Cases])==0; iLow = Cases<Nmin{ifile};
  iKeep = ~iSame & ~iLow; 
  
  Dates = Dates(iKeep);
  CountryData.(myCase).Value = Cases(iKeep);
  CountryData.(myCase).Dates = Dates;

  %
  if doplot
  subplot(3,1,ifile)
  Dates=CountryData.(myCase).Dates;
  plot(CountryData.(myCase).Dates,CountryData.(myCase).Value,'x','DisplayName',myCase); hold on;
  set(gca,'XTick',linspace(Dates(1),Dates(end),7))
  set(gca,'XTickLabel',datestr(get(gca,'XTick')))
  legend('show'); title(sprintf('%s - %s',country,myCase));
  end
end