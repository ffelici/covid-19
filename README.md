## COVID-19 Pandemic modeling in Matlab.

Model created based on SEIR model (Susceptible, Exposed, Infected, Recovered)
Equations found on [this Wikipedia page](https://en.wikipedia.org/wiki/Compartmental_models_in_epidemiology)

Data from [Johns Hopkins University Center for Systems Science and Engineering (JHU CSSE)](https://github.com/CSSEGISandData/COVID-19)

To fetch the data type `git submodule init && git submodule update`

## Disclaimer:
The author is not an epidemologist nor a healthcare professional. This model should not be relied on for any purpose whatsoever.

(c) F. Felici 2020

## Method:
A SEIR model was fitted to data of various countries using availble data and best guess of some unknown parameters. 

Then the pandemic is simulated for various degrees of effectiveness of the containment measures, i.e. varying the % of infections prevented by the 'social distancing' measures.

## Results:
**NB RESULTS ARE SUSCEPTIBLE TO MODELING/CODE/OTHER ERRORS**

* The main parameter that can not be fitted with the present data is the ratio of registered/true cases, which is very important to determine how long the pandemic will last and the effects of mitgitation. In the extreme case (~1%), we get a 'flu-like' disease which quickly affects a large part of the population but is of limited mortality. In case the ratio approaches (~50%) then strong efforts (>95% infection reduction) are required to reduce the contagion to avoid large amounts of casualities.
* Overview plots are shown in the [plots](/plots) folder.

### Example: Swiss situation evolution with 95% mitigation
![CH_95](/plots/Overview_CH_150320_contain_95.jpg)

### Example: Effect of containment measures depending on %registered/true cases
![CH_Cotainment](/plots/Containment_policy_effects_CH.jpg)

### Example: What 'flatten the curve' means for CH
![CH_Flatten](/plots/Flatten_the_curve_CH.jpg)

## Modeling details
There are 5 reservoirs:

* S (Susceptible)  - have not had the virus yet but can have it
* E (Exposed)  - caught the virus but shows no symptoms and is not infected
* I (Infected) - contagious
* R (Recovered) - Recovered from the virus
* D (Deceased)

Other quantities are defined as:

* G (Registered) - some fraction of I
* H (Hospitalized) - some fraction of G
* C (Require IC) - some fraction of H

It is assumed that the population remains constant during the Simulation, the sum is always $`S+E+I+R+D=N`$.

People transition between these reservoir following these equations:

* Susceptible people become exposed by interaction with ill people. Contagion coefficient $`\beta`$ can change depending on containment measures.
  * $`e_{new}  = \beta * I(t) (S(t)/N)`$ 
* Exposed people (E) become infected after incubation time 
  * $`e_{inf} =   \alpha E(t)`$ 
* Infected people (I) either recover or pass away, at different rates
  * $`i_{rec}  =  (1-f_{fatal}) * \gamma I(t)`$
  * $`i_{dec}  =     f_{fatal}  * \delta I(t)`$

Coefficients:

* $`\alpha`$ 1/time for exposed person becoming infected `1/Ti` where Ti is the incubation period.
* $`\beta`$ transmissions per infected person per day: `1/Tc` where Tc is the typical time between contacts. 
* $`\gamma`$ rate of recoveries per infected person that will recover `=1/Tr` with `Tr` the typical time of recovery.
* $`\delta`$ rate of fatalities per infected person tha twill eventually pass `=1/Td` with `Td` the typical time for passing away.
* $`f_{fatal}`$ is the number of infected cases that are fatal.
 
Model differential equations

  * $`dS/dt  = -e_{new}`$
  * $`dE/dt  =  e_{new} - e_{inf}`$
  * $`dI/dt  =  e_{inf} - i_{rec} - i_{dec}`$
  * $`dR/dt  =  i_{rec}`$
  * $`dD/dt  =  i_{dec}`$

The simulation is seeded with an initial $`S(t_0)`$ and small $`E(t_0)`$.

For more details see the simulation file `SEIRD_model.m`
