Feel free to contribute!

* Submit an issue to discuss what you want to add (avoid overlap/double work)
* Submit a Merge Request after your work is done.