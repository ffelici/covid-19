function out = SEIRD_model(varargin)
% model based on SEIR model equations found on Wikipedia,
% https://en.wikipedia.org/wiki/Compartmental_models_in_epidemiology
% adding 'Deceased' reservoir
%
% See README.md for equation details

% Country and population data
P.country = 'NL';       % country for data
P.fpop = 0.8;           % fraction of population susceptible

% Contagion parameters
P.Ti      = 7;          % Incubation time (not yet contagious) [Days]
P.Tc      = 1;          % Mean time between infectious contacts per infected person [Days]
P.Tr      = 10;         % Time for recovering [Days]
P.Td      = 5;          % Time for deceasing (once infected) [Days]
P.f_reg   = 0.01;       % fraction of infected people registering as sick (appear in statistics as confirmed cases)
P.f_hosp  = 0.2;        % fraction of registered cases needing hospital care
P.f_IC    = 0.2;        % fraction of hospital cases needing IC care
P.f_fatal = 0.01;       % fraction of registered cases becoming fatalities (Hospitals not overloaded)
P.f_fatov = [];         % fraction of infected people becoming fatalities (Hospitals overloaded)
% Containment parameters
P.f_contain = 0.95;      % fraction of infections prevented by containment

% Simulation parameters
P.Nsim   = 200;        % number of days of the simulation
P.Eseed  = 100;         % seed number of exposed cases
P.Istart = 150;         % number of ill people where to start storing time sequence
P.Istop  = 2;          % number of deceased people per day where to stop simulation

% cosmetics
P.disp   = 1;           % display each iteration
%% Get user custom country
if any(contains(varargin(1:2:end),'country'))
  ii = find(contains(varargin(1:2:end),'country'));
  P.country = varargin{2*ii};
end

%% Country data
fprintf('Running model for country %s\n',P.country);
[CountryData,CountryParams] = getCountryData(P.country);

%% replace country-specific parameters
for field = fieldnames(CountryParams)'
  myfield = field{1};
  assert(isfield(P,myfield),'invalid parameter name %s',myfield);
  P.(myfield) = CountryParams.(myfield);
end

%% Override with custom user input
for k = 1:2:length(varargin)
  assert(isfield(P,varargin{k}),'invalid parameter name %s', varargin{k});
  P.(varargin{k}) = varargin{k+1};
end

%% Initialize
N = CountryData.Population * P.fpop; % total people in model
[I,R,S,D,E,G,C,H,ECum,ICum,Date]=deal(zeros(P.Nsim,1));
E(1) = P.Eseed; % seed
S(1) = N-E(1)-I(1);

%% Modified SEIR model (Susceptible, Exposed, Infected(Ill), Recovered) model
it=1; itot=1; itotmax=1000*P.Nsim;
while it < P.Nsim && itot<itotmax
  %% Time-varying coefficients
  % Containment strategy enacted on given day, cuts beta by a factor
  beta0 = 1/P.Tc;
  if Date(it)>CountryData.DateContain
    beta_contain = (1-P.f_contain)*beta0;
    % it takes CountryData.Tcontain days before measures are fully effective
    lam = min((Date(it)-CountryData.DateContain)/CountryData.Tcontain,1);
    beta = beta_contain*lam + beta0*(1-lam); % linearly interpolate between beta0 and beta_contain
  else
    beta = 1/P.Tc;
  end
  
  % Fatality rate depending on hospital overloading
  if ~isempty(P.f_fatov)
    f_overload = 1./(1+exp(CountryData.HospitalCapacity-H(it))); % sigmoid)
    f_fatal = P.f_fatal*(1-f_overload) + P.f_fatov*f_overload;
  else
    f_fatal = P.f_fatal;
  end
  
  %% Updates
  % new exposed cases per day.
  exposed  = beta * I(it) * S(it) / N;
  
  % exposed people fall ill 
  alpha = 1/P.Ti;
  infected =  alpha * E(it);
  
  % ill people either recover or decease, at different rates%
  gamma = 1/P.Tr; delta = 1/P.Td;
  recovered  = (1-f_fatal*P.f_reg) * gamma * I(it); % ill people recovering
  deceased   =    f_fatal*P.f_reg  * delta * I(it); % ill people deceasing

  %% Start storing data after model passes threshold number of cases
  if G(it)>P.Istart || it>1
    it1 = it+1; % increment time counter
    if it==1
      % Assign dates to model based on time when data passes same threshold
      iStartData = find(CountryData.Confirmed.Value > P.Istart,1,'first')-1;
      Date(it) = CountryData.Confirmed.Dates(iStartData); % date of country data passing Istart infections
    end
    Date(it+1) = Date(it)+1;
  else
    it1 = it; itot = itot+1;
  end
  
  %% Model difference equations (per day)
  S(it1)  = S(it) - exposed; % susceptible
  E(it1)  = E(it) + exposed - infected; % exposed
  I(it1)  = I(it) + infected - recovered - deceased; % infected
  R(it1)  = R(it) + recovered; % recovered
  D(it1)  = D(it) + deceased; % deceased

  %% Non-state variables
  G(it1) = I(it)*P.f_reg;  % Infected cases registered
  H(it1) = G(it)*P.f_hosp; % Infected cases in hospital
  C(it1) = H(it)*P.f_IC;   % Patients needing IC
  
  ECum(it1) = ECum(it) + exposed; % cumulative number of exposed 
  ICum(it1) = ICum(it) + infected; % cumulative number of infected to date 

  %% On timer increment
  if it1== it+1
    it=it1;
    
    % display info
    if P.disp
      if mod(it,20)==0
        fprintf('\n%11s | %10s %10s %10s %10s %10s %10s\n','Date','Susceptible','Exposed','Infected','Recovered','Deceased','Confirmed (model)');
      end
      fprintf('%10s | %10.0f %10.0f %10.0f %10.0f %10.0f %10.0f\n',....
        datestr(Date(it)),S(it),E(it),I(it),R(it),D(it),C(it));
    end
    
    % Break if pandemic ends
    if it>50 && (all(abs(diff(D(it-10:it)))<P.Istop) || S(it)<0)
      fprintf('\nPandemic ended on %s. Deceased:%.1fk\n',datestr(Date(it)),D(it)/1000);
      break
    end
  end
end

% Sanity check on total number of people in model
N = S+E+I+R+D;
assert(norm(diff(N(1:it)))<1000,'Model does not maintain population')

%% outputs
out.t = Date(1:it); % Date
out.I = I(1:it); % Infected
out.E = E(1:it); % Exposed
out.R = R(1:it); % Recovered
out.S = S(1:it); % Susceptible
out.D = D(1:it); % Deceased
out.H = H(1:it); % Hospital
out.C = C(1:it); % in IC
out.G = G(1:it); % registered sick cases
out.ECum = ECum(1:it); % cumulative exposed cases
out.ICum = ICum(1:it); % cumulative infected cases
out.N = N; % total population in reservoirs
out.P = P; % parameters
out.CountryData = CountryData;

end


function [CountryData,CountryParam] = getCountryData(country)
% Country-specific data
switch country
  case 'NL'
    countryName = "Netherlands";
    CountryData = data_parser(countryName);
    CountryData.Population = 17e6;
    CountryData.Tcontain = 5; % days before containment is effective
    CountryData.ICBedCapacity = 500; % IC bed capacity
    CountryData.HospitalCapacity = 3/1000*CountryData.Population; % Hospital capacity
    CountryData.DateContain = datenum(2020,03,16);
    
    CountryParam.f_fatal = 0.01;
    CountryParam.f_reg   = 0.01;
  case 'IT'
    countryName = "Italy";
    CountryData = data_parser(countryName);
    CountryData.Population = 62e6;
    CountryData.Tcontain = 5;
    CountryData.ICBedCapacity = 1000; % IC bed capacity
    CountryData.HospitalCapacity = 3/1000*CountryData.Population; % Hospital capacity
    CountryData.DateContain = datenum(2020,03,08);
    
    CountryParam.f_fatal = 0.04;
    CountryParam.f_reg   = 0.01;
    CountryParam.f_hosp  = 0.1;
  case 'CH'
    countryName = "Switzerland";
    CountryData = data_parser(countryName);
    CountryData.Population = 8e6;
    CountryData.Tcontain = 3;
    CountryData.ICBedCapacity = 500; % IC bed capacity
    CountryData.HospitalCapacity = 3/1000*CountryData.Population; % Hospital capacity
    CountryData.DateContain = datenum(2020,03,13);
    
    CountryParam.f_fatal = 0.01;
    CountryParam.f_reg   = 0.01;
  otherwise
    error('country %s not defined',country);
end
end