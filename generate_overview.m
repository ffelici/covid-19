
countries = {'NL','IT','CH'};
plotdir = 'plots';

for f_contain = {0,0.95}
  for country = countries
    %%
    hf = plot_overview(country{1},'Nsim',100,'f_contain',f_contain{1});
    set(hf,'position',[50 50 900 500]);
    filename = fullfile(plotdir,sprintf('Overview_%s_%s_contain_%02.0f',...
      country{1},datestr(now,'ddmmyy'),f_contain{1}*100));
    fprintf('Saved %s\n',filename)
    set(hf,'paperpositionmode','auto');
    print('-djpeg',filename)
  end
end
